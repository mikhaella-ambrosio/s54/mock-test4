let collection = [];

// Write the queue functions below.


function print() {
    //Print queue elements.
    // empty array 
    return collection
}

function enqueue(name){
    collection.push(name)
    return collection
}

function dequeue(name){
    collection.shift(name)
    return collection
}

function front(){
return collection[0]
}

function size(){
    return collection.length
}

function isEmpty(){
    if(collection.length == 0){
        return true
    } else {
        return false
    }
}


// function 
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};